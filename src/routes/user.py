from typing import List

from fastapi import APIRouter
from cryptography.fernet import Fernet
from starlette.status import HTTP_204_NO_CONTENT

from ..config.db import conn
from ..models.user import users
from ..schemas.user import User, UserCreate


user = APIRouter(
    prefix="/user",
    tags=["User"],
)
key = Fernet.generate_key()
f = Fernet(key)


@user.get(
    "/",
    response_model=List[User],
    description="Get a list of all users",
)
def get_users():
    return conn.execute(users.select()).fetchall()


@user.get(
    "/{id}",
    response_model=User,
    description="Get a single user by Id",
)
def get_user(id: str):
    return conn.execute(users.select().where(users.c.id == id)).first()


@user.post(
    "/",
    response_model=User,
    description="Create a new user"
)
def create_user(user: UserCreate):
    new_user = {"name": user.name, "email": user.email}
    new_user["password"] = f.encrypt(user.password.encode("utf-8"))
    result = conn.execute(users.insert().values(new_user))
    return conn.execute(users.select().where(users.c.id == result.lastrowid)).first()


@user.put(
    "/{id}",
    response_model=User,
    description="Update a User by Id"
)
def update_user(user: User, id: int):
    conn.execute(
        users.update()
        .values(name=user.name, email=user.email, password=user.password)
        .where(users.c.id == id)
    )
    return conn.execute(users.select().where(users.c.id == id)).first()


@user.delete(
    "/{id}",
    description="Delete a User by Id",
    status_code=HTTP_204_NO_CONTENT,
)
def delete_user(id: int):
    conn.execute(users.delete().where(users.c.id == id))
    return conn.execute(users.select().where(users.c.id == id)).first()
