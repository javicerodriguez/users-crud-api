from fastapi import FastAPI
from .routes.user import user
from .config.openapi import tags_metadata

app = FastAPI(
    title="User's management API",
    description="REST API for user's management",
    version="0.0.1",
    openapi_tags=tags_metadata,
)

app.include_router(user)
